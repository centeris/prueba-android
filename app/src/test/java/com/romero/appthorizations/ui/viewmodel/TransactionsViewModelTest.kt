package com.romero.appthorizations.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.romero.appthorizations.domain.GetTransactionsUseCase
import com.romero.appthorizations.domain.model.Transaction
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class TransactionsViewModelTest {

    @RelaxedMockK
    private lateinit var getTransactionsUseCase: GetTransactionsUseCase

    private lateinit var transactionsViewModel: TransactionsViewModel


    @get:Rule
    var rule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun before() {
        MockKAnnotations.init(this)
        transactionsViewModel = TransactionsViewModel(getTransactionsUseCase)
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @After
    fun after() {
        Dispatchers.resetMain()
    }

    @Test
    fun `when getTransactions return a list transactions set on the livedata without searchText`() =
        runTest {

            val status = "Approved"
            val list = listOf(
                Transaction(
                    "1231-1233",
                    "234.00",
                    "3456",
                    "Approved",
                    "1233-1123",
                    "2022-05-09"
                )
            )
            coEvery { getTransactionsUseCase(status) } returns list

            transactionsViewModel.getTransactions(status)
            assert(transactionsViewModel.transaction_model.value == list)

        }

    @Test
    fun `when getTransactions return a list transactions set on the livedata with searchText`() =
        runTest {

            val status = "Approved"
            val receipt_id = "1233-1123"
            val list = listOf(
                Transaction(
                    "1231-1233",
                    "234.00",
                    "3456",
                    "Approved",
                    "1233-1123",
                    "2022-05-09"
                )
            )
            coEvery { getTransactionsUseCase(status, receipt_id) } returns list.filter { it.receipt_id.equals(receipt_id) }

            transactionsViewModel.getTransactions(status, receipt_id)
            assert(transactionsViewModel.transaction_model.value == list)
        }
}