package com.romero.appthorizations.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.romero.appthorizations.domain.GetCountersUseCase
import com.romero.appthorizations.domain.model.Counter
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class MainViewModelTest {
    @RelaxedMockK
    private lateinit var getCountersUseCase: GetCountersUseCase

    private lateinit var mainViewModel : MainViewModel


    @get:Rule
    var rule : InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun before() {
        MockKAnnotations.init(this)
        mainViewModel = MainViewModel(getCountersUseCase)
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @After
    fun after(){
        Dispatchers.resetMain()
    }

    @Test
    fun `when getCountersUseCase return a list counters set on the livedata`() = runTest {
        val counters = listOf(Counter("Approved", 1), Counter("Canceled", 3))
        coEvery { getCountersUseCase() } returns counters

        mainViewModel.getCounters()

        assert(mainViewModel.response_counters.value == counters)
    }
}