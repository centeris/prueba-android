package com.romero.appthorizations.domain

import com.romero.appthorizations.data.repositories.TransactionRepository
import com.romero.appthorizations.domain.model.Response
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class GetAuthorizationUseCaseTest{
    @RelaxedMockK
    private lateinit var transactionRepository: TransactionRepository

    private lateinit var getAuthorizationUseCase: GetAuthorizationUseCase

    @Before
    fun before(){
        MockKAnnotations.init(this)
        getAuthorizationUseCase = GetAuthorizationUseCase(transactionRepository)
    }

    @Test
    fun `when get authorization by transaction_id`() = runBlocking{
        val transaction_id = "1233-1234"
        val list = listOf(Response("1233-1234","1233-1123","5544-4455","00","Aprobado"))

        coEvery { transactionRepository.getAuthorizationByTransactionId(transaction_id) } returns list.first{ it.transaction_id.equals(transaction_id)}

        var response = getAuthorizationUseCase(transaction_id)

        coVerify (exactly = 1){ transactionRepository.getAuthorizationByTransactionId(transaction_id) }

        assert(response.transaction_id.equals(transaction_id))
    }
}