package com.romero.appthorizations.domain

import com.romero.appthorizations.data.repositories.TransactionRepository
import com.romero.appthorizations.domain.model.Transaction
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class GetTransactionsUseCaseTest {

    @RelaxedMockK
    private lateinit var transactionRepository: TransactionRepository

    lateinit var getTransactionUseCase: GetTransactionsUseCase

    @Before
    fun before(){
        MockKAnnotations.init(this)
        getTransactionUseCase = GetTransactionsUseCase(transactionRepository)
    }

    @Test
    fun `when not has param search`() = runBlocking{
        val status = "Approved"
        val receipt_id = "1233-1123"

        coEvery { transactionRepository.getTransactions(status) } returns emptyList()

        getTransactionUseCase(status)

        coVerify (exactly = 1){ transactionRepository.getTransactions(status) }
        coVerify (exactly = 0){ transactionRepository.getTransactionsByReceiptId(status,receipt_id) }
    }

    @Test
    fun `when has param search`() = runBlocking{
        val status = "Approved"
        val receipt_id = "1233-1123"
        val list = listOf(Transaction("1231-1233","234.00","3456","Approved","1233-1123","2022-05-09"))

        coEvery { transactionRepository.getTransactionsByReceiptId(status, receipt_id) } returns list

        val response = getTransactionUseCase(status,receipt_id)

        coVerify (exactly = 0){ transactionRepository.getTransactions(status) }
        coVerify (exactly = 1){ transactionRepository.getTransactionsByReceiptId(status,receipt_id) }

        assert(list === response)
    }
}