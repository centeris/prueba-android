package com.romero.appthorizations.domain

import com.romero.appthorizations.data.repositories.TransactionRepository
import com.romero.appthorizations.domain.model.Counter
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class GetCountersUseCaseTest {

    @RelaxedMockK
    private lateinit var transactionRepository: TransactionRepository

    private lateinit var getCountersUseCase: GetCountersUseCase

    @Before
    fun before() {
        MockKAnnotations.init(this)
        getCountersUseCase = GetCountersUseCase(transactionRepository)
    }

    @Test
    fun `retrieve list of counters`() = runBlocking {

        val counters = listOf(Counter("Approved", 1), Counter("Canceled", 3))

        coEvery { transactionRepository.getCounters() } returns counters

        val response = getCountersUseCase()

        coVerify(exactly = 1) { transactionRepository.getCounters() }

        assert(counters == response)
    }
}