package com.romero.appthorizations.data.db

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.romero.appthorizations.data.db.dao.DB_ResponseDao
import com.romero.appthorizations.data.db.dao.DB_TransactionDao
import com.romero.appthorizations.data.db.entities.DB_Response
import com.romero.appthorizations.data.db.entities.DB_Transaction

@Database(
    entities = [
        DB_Transaction::class,
        DB_Response::class
    ],
    version = 1
)
abstract class Appthorizations : RoomDatabase(){
    abstract fun transactionDao() : DB_TransactionDao
    abstract fun responseDao() : DB_ResponseDao
}