package com.romero.appthorizations.data.repositories

import android.content.Context
import com.romero.appthorizations.R
import com.romero.appthorizations.core.SweetAlert
import com.romero.appthorizations.data.db.dao.DB_ResponseDao
import com.romero.appthorizations.data.db.dao.DB_TransactionDao
import com.romero.appthorizations.data.db.entities.DB_Response
import com.romero.appthorizations.data.db.entities.DB_Transaction
import com.romero.appthorizations.data.network.TransactionService
import com.romero.appthorizations.data.network.request.AnnulmentRequest
import com.romero.appthorizations.data.network.request.AuthorizationRequest
import com.romero.appthorizations.data.network.response.AuthorizationResponse
import com.romero.appthorizations.data.network.response.BaseResponse
import com.romero.appthorizations.domain.model.Counter
import com.romero.appthorizations.domain.model.Response
import com.romero.appthorizations.domain.model.Transaction
import com.romero.appthorizations.domain.model.toDomain
import dagger.hilt.android.qualifiers.ApplicationContext
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class TransactionRepository @Inject constructor(
    @ApplicationContext private val context: Context,
    private val transaction_service: TransactionService,
    private val transaction_dao: DB_TransactionDao,
    private val response_dao: DB_ResponseDao,
) {

    suspend fun sendAuthorization(amount: String, card: String): SweetAlert.Alert? {
        val parse_amount = "%.2f".format(Locale.ENGLISH, amount.toFloat())
        val authorization = AuthorizationRequest(
            UUID.randomUUID().toString(),
            context.getString(R.string.commerceCode),
            context.getString(R.string.terminalCode),
            parse_amount.replace(".", ""),
            card
        )

        var response = transaction_service.sendAuthorization(authorization)
        var transaction = DB_Transaction(
            authorization.id, parse_amount, card.substring(card.length - 4, card.length),
            SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                .format(Calendar.getInstance().getTime())
        )

        if (response != null) {
            if (response is AuthorizationResponse) {
                transaction.status = "Approved"
                transaction_dao.insert(transaction)
                response_dao.insert(
                    DB_Response(
                        transaction.id,
                        response.receipt_id,
                        response.rrn,
                        response.status_code,
                        response.status_description
                    )
                )
                return SweetAlert.Alert(
                    "Success",
                    "Éxito",
                    "Se ha autorizado el monto $${transaction.amount} correctamente",
                    true
                )
            } else if (response is SweetAlert.Alert) {
                if (response.type.equals("Error")) {
                    transaction.status = "Denied"
                    transaction_dao.insert(transaction)
                    response_dao.insert(
                        DB_Response(
                            transaction.id,
                            null,
                            null,
                            "99",
                            response.text
                        )
                    )
                }
                return response
            }
        }
        return null
    }

    suspend fun sendAnnulment(transaction: Transaction): SweetAlert.Alert? {
        val approvedResponse = response_dao.getApprovedResponseByTransactionId(transaction.id)
        val annulment = AnnulmentRequest(approvedResponse.receipt_id, approvedResponse.rrn)

        var response = transaction_service.sendAnnulment(annulment)

        if (response != null) {
            if (response is BaseResponse) {
                transaction_dao.updateStatus(transaction.id, "Canceled")
                response_dao.insert(
                    DB_Response(
                        transaction.id,
                        null,
                        null,
                        response.status_code,
                        response.status_description
                    )
                )
                return SweetAlert.Alert(
                    "Success",
                    "Éxito",
                    "Se ha cancelado la autorización por $${transaction.amount} correctamente",
                    true
                )
            } else if (response is SweetAlert.Alert) {
                if (response.type.equals("Error")) {
                    response_dao.insert(
                        DB_Response(
                            transaction.id,
                            null,
                            null,
                            "99",
                            response.text
                        )
                    )
                }
                return response
            }
        }
        return null
    }

    suspend fun getTransactions(status: String): List<Transaction> {
        return transaction_dao.getTransactions(status)
    }

    suspend fun getTransactionsByReceiptId(status: String, searchText: String): List<Transaction> {
        return transaction_dao.getTransactionsByReceiptId(status, searchText)
    }

    suspend fun getAuthorizationByTransactionId(transaction_id: String): Response {
        return response_dao.getApprovedResponseByTransactionId(transaction_id).toDomain()
    }

    suspend fun getCounters(): List<Counter> {
        return transaction_dao.getCounters()
    }
}