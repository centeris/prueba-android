package com.romero.appthorizations.data.network

import com.romero.appthorizations.data.network.request.AnnulmentRequest
import com.romero.appthorizations.data.network.request.AuthorizationRequest
import com.romero.appthorizations.data.network.response.AuthorizationResponse
import com.romero.appthorizations.data.network.response.BaseResponse
import retrofit2.Response
import retrofit2.http.*

interface AppthorizationsAPI {

    @POST("authorization")
    suspend fun authorization(@Body authorization: AuthorizationRequest): Response<AuthorizationResponse>

    @POST("annulment")
    suspend fun annulment(@Body annulment: AnnulmentRequest): Response<BaseResponse>
}