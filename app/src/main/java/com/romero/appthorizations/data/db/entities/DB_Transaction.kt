package com.romero.appthorizations.data.db.entities


import androidx.room.*
import com.romero.appthorizations.data.network.request.AuthorizationRequest
import java.io.Serializable

@Entity(tableName = "transactions")
class DB_Transaction : Serializable {
    @PrimaryKey(autoGenerate = false)
    var id: String = ""
    var amount: String? = null
    var last4: String? = null
    var status: String? = null
    var createdAt: String? = ""

    constructor(id: String, amount: String?, last4: String?, createdAt: String? = null, status: String? = null) {
        this.id = id
        this.amount = amount
        this.last4 = last4
        this.createdAt = createdAt
        this.status = status
    }
}