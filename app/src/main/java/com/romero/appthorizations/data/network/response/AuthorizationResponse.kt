package com.romero.appthorizations.data.network.response

import com.google.gson.annotations.SerializedName

class AuthorizationResponse : BaseResponse() {
    @SerializedName("receiptId")
    val receipt_id: String = ""

    @SerializedName("rrn")
    val rrn: String = ""
}
