package com.romero.appthorizations.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.romero.appthorizations.data.db.entities.DB_Response
import com.romero.appthorizations.data.db.entities.DB_Transaction

@Dao
interface DB_ResponseDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(db_response: DB_Response)

    @Query("SELECT * FROM responses where transaction_id = :id and statusCode = '00'")
    suspend fun getApprovedResponseByTransactionId(id : String): DB_Response
}