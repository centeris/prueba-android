package com.romero.appthorizations.data.network.response

import com.google.gson.annotations.SerializedName

open class BaseResponse {

    @SerializedName("statusCode")
    val status_code: String = ""
    @SerializedName("statusDescription")
    val status_description: String = ""
}
