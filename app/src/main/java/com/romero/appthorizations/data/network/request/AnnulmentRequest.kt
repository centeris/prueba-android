package com.romero.appthorizations.data.network.request

data class AnnulmentRequest(
    val receiptId : String?,
    val rrn: String?
    )
