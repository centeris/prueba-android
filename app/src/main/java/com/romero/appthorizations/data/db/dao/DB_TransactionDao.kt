package com.romero.appthorizations.data.db.dao

import androidx.room.*
import com.romero.appthorizations.data.db.entities.DB_Transaction
import com.romero.appthorizations.domain.model.Counter
import com.romero.appthorizations.domain.model.Transaction

@Dao
interface DB_TransactionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(db_transactions: List<DB_Transaction>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(db_transaction: DB_Transaction)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(db_transaction: DB_Transaction)

    @Query("Update transactions set status = :status where id = :id")
    suspend fun updateStatus(id: String, status: String)

    @Query("SELECT transactions.*,responses.receipt_id FROM transactions inner join responses on transactions.id = transaction_id where statusCode = '00' and receipt_id is not null and status = :status order by createdAt desc")
    suspend fun getTransactions(status : String): List<Transaction>

    @Query("SELECT transactions.*,responses.receipt_id FROM transactions inner join responses on transactions.id = transaction_id where statusCode = '00' and receipt_id is not null and status = :status and receipt_id like :searchText order by createdAt desc")
    suspend fun getTransactionsByReceiptId(status : String,searchText: String): List<Transaction>

    @Query("SELECT transactions.status as name,count(*) as value FROM transactions inner join responses on transactions.id = transaction_id where statusCode = '00' and receipt_id is not null group by status")
    suspend fun getCounters(): List<Counter>
}