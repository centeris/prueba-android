package com.romero.appthorizations.data.network

import android.content.Context
import com.romero.appthorizations.core.SweetAlert
import com.romero.appthorizations.data.network.request.AnnulmentRequest
import com.romero.appthorizations.data.network.request.AuthorizationRequest
import com.romero.appthorizations.data.network.response.AuthorizationResponse
import com.romero.appthorizations.data.network.response.BaseResponse
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.net.ConnectException
import javax.inject.Inject

class TransactionService @Inject constructor(
    private val api: AppthorizationsAPI,
    private var alert: SweetAlert,
    @ApplicationContext val context: Context
) {

    suspend fun sendAuthorization(authorizationRequest: AuthorizationRequest): Any? {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.authorization(authorizationRequest)
                if (response.isSuccessful) {
                    response.body()
                } else {
                    SweetAlert.Alert(
                        "Error",
                        "Error",
                        getErrorResponse(response.errorBody()!!.string())
                    )
                }
            } catch (e: ConnectException) {
                e.printStackTrace()
                SweetAlert.Alert("Info", "Atención", "No hay conexión a internet")
            }
        }
    }

    suspend fun sendAnnulment(annulmentRequest: AnnulmentRequest): Any? {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.annulment(annulmentRequest)
                if (response.isSuccessful) {
                    response.body()
                } else {
                    SweetAlert.Alert(
                        "Error",
                        "Error",
                        getErrorResponse(response.errorBody()!!.string())
                    )
                }
            } catch (e: ConnectException) {
                e.printStackTrace()
                SweetAlert.Alert("Info", "Atención", "No hay conexión a internet")
            }
        }
    }

    fun getErrorResponse(errorBody: String): String {
        try {
            val jObjError = JSONObject(errorBody)
            if (jObjError != null && jObjError.has("statusDescription")) {
                return jObjError.getString("statusDescription")
            }
        } catch (e: Exception) {
        }
        return errorBody
    }
}