package com.romero.appthorizations.data.db.entities


import androidx.room.*
import java.io.Serializable

@Entity(
    tableName = "responses",
    foreignKeys = [
        ForeignKey(
            entity = DB_Transaction::class,
            parentColumns = ["id"],
            childColumns = ["transaction_id"], onDelete = ForeignKey.SET_DEFAULT
        )
    ]
)
class DB_Response : Serializable {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
    @ColumnInfo(defaultValue = "1")
    var transaction_id: String = "0"
    var receipt_id: String? = null
    var rrn: String? = null
    var statusCode: String? = null
    var statusDescription: String? = null

    constructor(
        transaction_id: String,
        receipt_id: String?,
        rrn: String?,
        statusCode: String?,
        statusDescription: String?
    ) {
        this.transaction_id = transaction_id
        this.receipt_id = receipt_id
        this.rrn = rrn
        this.statusCode = statusCode
        this.statusDescription = statusDescription
    }
}