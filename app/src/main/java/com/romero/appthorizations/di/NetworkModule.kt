package com.romero.appthorizations.di

import android.content.Context
import android.util.Base64
import com.romero.appthorizations.R
import com.romero.appthorizations.data.network.AppthorizationsAPI
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit(@ApplicationContext context: Context): AppthorizationsAPI {
        val client = OkHttpClient.Builder()
        val authorization =
            context.resources.getString(R.string.commerceCode) + context.resources?.getString(
                R.string.terminalCode
            )
        client.addInterceptor { chain ->
            val original = chain.request()

            val request = original.newBuilder()
                .header(
                    "Authorization",
                    "Basic ${Base64.encodeToString(authorization.toByteArray(), Base64.DEFAULT).trim()}"
                )
                .method(original.method, original.body)
                .build()

            chain.proceed(request)
        }

        return Retrofit.Builder()
            .baseUrl(context.resources?.getString(R.string.base_url) ?: "")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client.build())
            .build()
            .create(AppthorizationsAPI::class.java)
    }
}