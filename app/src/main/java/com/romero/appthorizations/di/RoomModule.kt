package com.romero.appthorizations.di

import android.content.Context
import androidx.room.Room
import com.romero.appthorizations.data.db.Appthorizations
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    private const val APPTHORIZATIONS_DATABASE_NAME = "appthorizations"

    @Singleton
    @Provides
    fun provideRoom(@ApplicationContext context : Context) =
        Room.databaseBuilder(context, Appthorizations::class.java, APPTHORIZATIONS_DATABASE_NAME).build()

    @Singleton
    @Provides
    fun provideTransactionDao(db : Appthorizations) = db.transactionDao()

    @Singleton
    @Provides
    fun provideResponseDao(db : Appthorizations) = db.responseDao()
}