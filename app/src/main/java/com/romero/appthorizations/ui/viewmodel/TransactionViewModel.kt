package com.romero.appthorizations.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.romero.appthorizations.core.SweetAlert
import com.romero.appthorizations.domain.CancelTransactionUseCase
import com.romero.appthorizations.domain.GetAuthorizationUseCase
import com.romero.appthorizations.domain.model.Response
import com.romero.appthorizations.domain.model.Transaction
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TransactionViewModel @Inject constructor(
    private val getAuthorizationUseCase: GetAuthorizationUseCase,
    private val cancelTransactionUseCase: CancelTransactionUseCase,
) : ViewModel() {

    val response_model = MutableLiveData<Response>()
    val showAlert = MutableLiveData<SweetAlert.Alert?>()

    fun getAuthorizationByTransactionId(transaction_id : String) {
        viewModelScope.launch {
            response_model.postValue(getAuthorizationUseCase(transaction_id))
        }
    }

    fun cancelTransaction(transaction: Transaction){
        viewModelScope.launch {
            showAlert.postValue(SweetAlert.Alert("Loading","Cargando"))
            try {
                showAlert.postValue(cancelTransactionUseCase(transaction))
            }catch (e : Exception){
                showAlert.postValue(SweetAlert.Alert("Info", "Atención", "No se ha establecido conexión con el servidor"))
            }
        }
    }
}