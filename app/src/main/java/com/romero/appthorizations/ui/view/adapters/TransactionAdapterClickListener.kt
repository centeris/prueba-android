package com.romero.appthorizations.ui.view.adapters

import com.romero.appthorizations.domain.model.Transaction

interface TransactionAdapterClickListener {

    fun showTransaction(transaction: Transaction)
}