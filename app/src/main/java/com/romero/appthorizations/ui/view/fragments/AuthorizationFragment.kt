package com.romero.appthorizations.ui.view.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.romero.appthorizations.core.SweetAlert
import com.romero.appthorizations.databinding.FragmentAuthorizationBinding
import com.romero.appthorizations.ui.view.MainActivity
import com.romero.appthorizations.ui.viewmodel.AuthorizationViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class AuthorizationFragment : Fragment() {

    private val authorizationViewModel: AuthorizationViewModel by viewModels()
    private var _binding: FragmentAuthorizationBinding? = null
    private val binding get() = _binding!!
    private lateinit var mainActivity: MainActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAuthorizationBinding.inflate(inflater, container, false)
        mainActivity = activity as MainActivity
        mainActivity.setSupportActionBar(binding.mainToolbar)

        binding.mainToolbar.setNavigationOnClickListener { mainActivity.onBackPressed() }

        binding.authorize.setOnClickListener {
            val amount = binding.amount.text.toString().trim()
            val card = binding.card.text.toString().trim()
            if (amount.isEmpty()) {
                mainActivity.showAlert(SweetAlert.Alert("Info","Atención", "Debe ingresar un monto"))
            }else if (card.length < 16) {
                mainActivity.showAlert(SweetAlert.Alert("Info","Atención", "Debe ingresar una tarjeta válida"))
            }else {
                authorizationViewModel.sendAuthorization(amount, card)
            }
        }

        authorizationViewModel.showAlert.observe(mainActivity, Observer {
            mainActivity.showAlert(it)
        })
        return binding.root
    }
}