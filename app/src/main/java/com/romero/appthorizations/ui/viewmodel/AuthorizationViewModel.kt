package com.romero.appthorizations.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.romero.appthorizations.core.SweetAlert
import com.romero.appthorizations.domain.SendAuthorizationUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AuthorizationViewModel
@Inject constructor(
    private val sendAuthorizationUseCase: SendAuthorizationUseCase
): ViewModel(){

    val showAlert = MutableLiveData<SweetAlert.Alert?>()

    fun sendAuthorization(amount : String, card : String){
        viewModelScope.launch {
            showAlert.postValue(SweetAlert.Alert("Loading","Cargando"))
            try {
                showAlert.postValue(sendAuthorizationUseCase(amount, card))
            }catch (e : Exception){
                showAlert.postValue(SweetAlert.Alert("Info", "Atención", "No se ha establecido conexión con el servidor"))
            }
        }
    }
}