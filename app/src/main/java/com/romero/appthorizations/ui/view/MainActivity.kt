package com.romero.appthorizations.ui.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.romero.appthorizations.core.SweetAlert
import com.romero.appthorizations.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    lateinit var alert: SweetAlert

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        alert = SweetAlert(this)
        setContentView(binding.root)
    }

    fun showAlert(alert_data: SweetAlert.Alert?){
        if (alert_data != null) {
            when (alert_data.type) {
                "Loading" -> alert.loadingAlert(alert_data.title!!)
                "Success" -> alert.successAlert(alert_data.title!!, alert_data.text!!, if(alert_data.back) this else null)
                "Error" -> alert.errorAlert(alert_data.title!!, alert_data.text!!)
                "Info" -> alert.infoAlert(alert_data.title!!, alert_data.text!!)
            }
        } else {
            alert.dismissDialog()
        }
    }
}