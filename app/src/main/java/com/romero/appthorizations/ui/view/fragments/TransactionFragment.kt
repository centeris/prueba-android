package com.romero.appthorizations.ui.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import cn.pedant.SweetAlert.SweetAlertDialog
import com.romero.appthorizations.databinding.FragmentTransactionBinding
import com.romero.appthorizations.domain.model.Transaction
import com.romero.appthorizations.ui.view.MainActivity
import com.romero.appthorizations.ui.viewmodel.TransactionViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class TransactionFragment : Fragment() {

    private val transaction_view_model: TransactionViewModel by viewModels()
    val args: TransactionFragmentArgs by navArgs()
    private var _binding: FragmentTransactionBinding? = null
    private val binding get() = _binding!!
    private lateinit var transaction : Transaction
    private lateinit var mainActivity: MainActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTransactionBinding.inflate(inflater, container, false)
        mainActivity = activity as MainActivity
        transaction = args.transaction as Transaction

        mainActivity.setSupportActionBar(binding.mainToolbar)

        binding.mainToolbar.setNavigationOnClickListener { mainActivity.onBackPressed() }

        binding.id.text = transaction.id
        binding.card.text = "**** ${transaction.last4}"
        binding.amount.text = "$${transaction.amount}"

        transaction_view_model.getAuthorizationByTransactionId(transaction.id)
        transaction_view_model.response_model.observe(mainActivity, Observer {
            binding.status.text = it.statusDescription
            binding.rrn.text = it.rrn
            binding.receipt.text = it.receipt_id
        })

        transaction_view_model.showAlert.observe(mainActivity, Observer {
                mainActivity.showAlert(it)
            })

        binding.cancel.setOnClickListener {
            try {
                val pDialog = SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                pDialog.cancelText = "No"
                pDialog.confirmText = "Si"
                pDialog.setCancelClickListener { sDialog ->
                    sDialog.cancel()
                }
                pDialog.titleText = "Cancelar Transacción"
                pDialog.contentText =
                    "Realmente desea cancelar el monto autorizado por $${transaction.amount} de la tarjeta ***${transaction.last4}"
                pDialog.setConfirmClickListener { sDialog ->
                    sDialog.cancel()
                    transaction_view_model.cancelTransaction(transaction)
                }
                pDialog.setCancelable(false)
                pDialog.show()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return binding.root
    }

}