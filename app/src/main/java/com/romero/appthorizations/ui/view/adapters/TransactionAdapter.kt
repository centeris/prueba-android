package com.romero.appthorizations.ui.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.romero.appthorizations.R
import com.romero.appthorizations.core.SweetAlert
import com.romero.appthorizations.databinding.ItemTransactionBinding
import com.romero.appthorizations.domain.model.Transaction
import com.romero.appthorizations.domain.model.parseStatus

class TransactionAdapter(
    private var transactions : List<Transaction>,
    private val transactionListener: TransactionAdapterClickListener
    ) : RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>() {

    var alert : SweetAlert? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        alert = SweetAlert(parent.context)
        return TransactionViewHolder(layoutInflater.inflate(R.layout.item_transaction, parent, false))
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        val transaction = transactions[position]
        holder.bind(transaction)
        holder.binding.transaction.setOnClickListener {
            if (transaction.status.equals("Approved")) {
                transactionListener.showTransaction(transaction)
            }
        }
    }

    override fun getItemCount(): Int = transactions.size


    class TransactionViewHolder(view : View) : RecyclerView.ViewHolder(view) {

        val binding = ItemTransactionBinding.bind(view)
        fun bind(transaction : Transaction){
            binding.amount.text = "$${transaction.amount}"
            binding.card.text = "***${transaction.last4}"
            binding.date.text = transaction.createdAt!!.split(" ")[0]
            binding.receipt.text = transaction.receipt_id
        }
    }
}