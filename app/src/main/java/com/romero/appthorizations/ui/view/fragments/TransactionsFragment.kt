package com.romero.appthorizations.ui.view.fragments

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.romero.appthorizations.R
import com.romero.appthorizations.core.SweetAlert
import com.romero.appthorizations.databinding.FragmentTransactionsBinding
import com.romero.appthorizations.domain.model.Transaction
import com.romero.appthorizations.ui.view.MainActivity
import com.romero.appthorizations.ui.view.adapters.TransactionAdapter
import com.romero.appthorizations.ui.view.adapters.TransactionAdapterClickListener
import com.romero.appthorizations.ui.viewmodel.TransactionsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TransactionsFragment : TransactionAdapterClickListener, Fragment() {

    private var _binding: FragmentTransactionsBinding? = null
    private val binding get() = _binding!!
    private lateinit var mainActivity: MainActivity
    private val transaction_view_model: TransactionsViewModel by viewModels()
    private lateinit var adapter: TransactionAdapter
    val args: TransactionsFragmentArgs by navArgs()
    private val transactions = mutableListOf<Transaction>()
    private var lastSearch = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (args.typeTransaction.equals("Approved"))
            setHasOptionsMenu(true)
    }

    override fun onResume() {
        super.onResume()
        lastSearch = ""
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTransactionsBinding.inflate(inflater, container, false)
        mainActivity = activity as MainActivity
        mainActivity.setSupportActionBar(binding.mainToolbar)
        initRecyclerView()


        if (args.typeTransaction.equals("Approved")) {
            binding.title.text = "Listado de transacciones Aprobadas"
        }else{
            binding.title.text = "Listado de transacciones Canceladas"
        }

        binding.mainToolbar.setNavigationOnClickListener { mainActivity.onBackPressed() }
        transaction_view_model.getTransactions(args.typeTransaction)
        transaction_view_model.transaction_model.observe(mainActivity, Observer {
            if (lastSearch.isNotEmpty() && lastSearch.isNotBlank() && it.isEmpty()) {
                mainActivity.showAlert(
                    SweetAlert.Alert(
                        "Error",
                        "Atención",
                        "No se ha encontrado una transacción con el Número de recibo $lastSearch"
                    )
                )
            } else if (lastSearch.isNotEmpty() && it.filter { t -> t.status.equals("Approved") }.size == 1) {
                findNavController().navigate(
                    TransactionsFragmentDirections.actionTransactionsFragmentToTransactionFragment(
                        it.first { t -> t.status.equals("Approved") }
                    )
                )
            } else {
                mainActivity.alert.dismissDialog()
                transactions.clear()
                transactions.addAll(it)
                binding.message.isVisible = it.isEmpty()
                adapter.notifyDataSetChanged()
            }
        })

        return binding.root
    }

    private fun initRecyclerView() {
        adapter = TransactionAdapter(transactions, this)
        binding.listTransactions.layoutManager = LinearLayoutManager(context)
        binding.listTransactions.adapter = adapter
    }

    override fun showTransaction(transaction: Transaction) {
        findNavController().navigate(
            TransactionsFragmentDirections.actionTransactionsFragmentToTransactionFragment(
                transaction
            )
        )
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.main, menu)
        var menuItem = menu.findItem(R.id.search_icon)
        var searchView = menuItem.actionView as SearchView
        searchView.queryHint = "Buscar"

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                lastSearch = "$query"
                transaction_view_model.getTransactions(args.typeTransaction, "$query")
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                return true
            }
        })
        super.onCreateOptionsMenu(menu, menuInflater)
    }
}