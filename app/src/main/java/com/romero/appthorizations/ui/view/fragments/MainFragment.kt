package com.romero.appthorizations.ui.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.romero.appthorizations.R
import com.romero.appthorizations.databinding.FragmentMainBinding
import com.romero.appthorizations.databinding.FragmentTransactionBinding
import com.romero.appthorizations.databinding.FragmentTransactionsBinding
import com.romero.appthorizations.domain.model.Counter
import com.romero.appthorizations.ui.view.MainActivity
import com.romero.appthorizations.ui.viewmodel.MainViewModel
import com.romero.appthorizations.ui.viewmodel.TransactionsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val main_view_model: MainViewModel by viewModels()
    private val binding get() = _binding!!
    private lateinit var mainActivity: MainActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        mainActivity = activity as MainActivity
        main_view_model.getCounters()

        main_view_model.response_counters.observe(mainActivity, Observer {
            binding.countApproved.text = retrieveCounter(it, "Approved")
            binding.countCanceled.text = retrieveCounter(it, "Canceled")
        })

        binding.newAuthorization.setOnClickListener {
            findNavController().navigate(MainFragmentDirections.actionMainFragmentToAuthorizationFragment())
        }

        binding.cardApproved.setOnClickListener {
            findNavController().navigate(MainFragmentDirections.actionMainFragmentToTransactionsFragment("Approved"))
        }

        binding.cardCanceled.setOnClickListener {
            findNavController().navigate(MainFragmentDirections.actionMainFragmentToTransactionsFragment("Canceled"))
        }

        return binding.root
    }

    fun retrieveCounter(list : List<Counter>, key : String) : String{
        val item = list.find { it.name.equals(key) }
        if (item != null)
            return "${item.value}"
        return "0"
    }
}