package com.romero.appthorizations.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.romero.appthorizations.core.SweetAlert
import com.romero.appthorizations.domain.CancelTransactionUseCase
import com.romero.appthorizations.domain.GetAuthorizationUseCase
import com.romero.appthorizations.domain.GetCountersUseCase
import com.romero.appthorizations.domain.model.Counter
import com.romero.appthorizations.domain.model.Response
import com.romero.appthorizations.domain.model.Transaction
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val getCountersUseCase: GetCountersUseCase,
) : ViewModel() {

    val response_counters = MutableLiveData<List<Counter>>()

    fun getCounters() {
        viewModelScope.launch {
            response_counters.postValue(getCountersUseCase())
        }
    }
}