package com.romero.appthorizations.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.romero.appthorizations.core.SweetAlert
import com.romero.appthorizations.domain.CancelTransactionUseCase
import com.romero.appthorizations.domain.GetTransactionsUseCase
import com.romero.appthorizations.domain.model.Transaction
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TransactionsViewModel @Inject constructor(
    private val getTransactionsUseCase: GetTransactionsUseCase
) : ViewModel() {

    val transaction_model = MutableLiveData<List<Transaction>>()

    fun getTransactions(status : String, searchText : String = "") {
        viewModelScope.launch {
            transaction_model.postValue(getTransactionsUseCase(status, searchText))
        }
    }
}