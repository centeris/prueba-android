package com.romero.appthorizations.domain.model

import com.romero.appthorizations.data.db.entities.DB_Response
import java.io.Serializable

data class Response(
    var transaction_id: String = "0",
    var receipt_id: String? = null,
    var rrn: String? = null,
    var statusCode: String? = null,
    var statusDescription: String? = null
)

fun DB_Response.toDomain() =
    Response(transaction_id, receipt_id, rrn, statusCode, statusDescription)