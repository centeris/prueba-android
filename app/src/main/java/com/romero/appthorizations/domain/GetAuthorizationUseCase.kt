package com.romero.appthorizations.domain


import com.romero.appthorizations.data.repositories.TransactionRepository
import com.romero.appthorizations.domain.model.Response
import javax.inject.Inject

class GetAuthorizationUseCase @Inject constructor(
    private val transactionRepository: TransactionRepository
) {

    suspend operator fun invoke(transaction_id : String): Response {
        return transactionRepository.getAuthorizationByTransactionId(transaction_id)
    }
}