package com.romero.appthorizations.domain

import com.romero.appthorizations.core.SweetAlert
import com.romero.appthorizations.data.repositories.TransactionRepository
import com.romero.appthorizations.domain.model.Transaction
import javax.inject.Inject

class CancelTransactionUseCase @Inject constructor(
    private val transactionRepository: TransactionRepository
) {
    suspend operator fun invoke(transaction: Transaction): SweetAlert.Alert? {
        return transactionRepository.sendAnnulment(transaction)
    }
}