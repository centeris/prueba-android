package com.romero.appthorizations.domain

import com.romero.appthorizations.data.repositories.TransactionRepository
import com.romero.appthorizations.domain.model.Transaction
import javax.inject.Inject

class GetTransactionsUseCase @Inject constructor(
    private val transactionRepository: TransactionRepository
) {

    suspend operator fun invoke(status: String, searchText: String = ""): List<Transaction> {
        if (searchText.isEmpty())
            return transactionRepository.getTransactions(status)
        return transactionRepository.getTransactionsByReceiptId(status,searchText)
    }
}