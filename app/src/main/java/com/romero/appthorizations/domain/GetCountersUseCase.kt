package com.romero.appthorizations.domain


import com.romero.appthorizations.data.repositories.TransactionRepository
import com.romero.appthorizations.domain.model.Counter
import javax.inject.Inject

class GetCountersUseCase @Inject constructor(
    private val transactionRepository: TransactionRepository
) {

    suspend operator fun invoke(): List<Counter> {
        return transactionRepository.getCounters()
    }
}