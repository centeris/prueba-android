package com.romero.appthorizations.domain.model

import java.io.Serializable

data class Transaction(
    val id: String,
    var amount: String?,
    var last4: String?,
    var status: String?,
    var receipt_id: String?,
    var createdAt: String?
) : Serializable

fun Transaction.parseStatus() : String{
    when(status){
        "Approved" -> return "Aprobado"
        "Denied" -> return "Denegado"
        "Canceled" -> return "Anulado"
    }
    return status ?: ""
}