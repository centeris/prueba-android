package com.romero.appthorizations.domain.model

data class Counter(
    var name: String = "",
    var value: Int = 0
)
