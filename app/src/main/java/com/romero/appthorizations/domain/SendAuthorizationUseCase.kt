package com.romero.appthorizations.domain

import com.romero.appthorizations.core.SweetAlert
import com.romero.appthorizations.data.repositories.TransactionRepository
import javax.inject.Inject

class SendAuthorizationUseCase @Inject constructor(
    private val transactionRepository: TransactionRepository
) {

    suspend operator fun invoke(amount : String, card : String): SweetAlert.Alert? {
        return transactionRepository.sendAuthorization(amount,card)
    }
}