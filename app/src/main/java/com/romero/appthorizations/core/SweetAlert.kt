package com.romero.appthorizations.core

import android.content.Context
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import cn.pedant.SweetAlert.SweetAlertDialog
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SweetAlert @Inject constructor( @ApplicationContext val context: Context) {

    var dialog: SweetAlertDialog? = null

    fun loadingAlert(text: String, cancelable: Boolean = false) {
        try {
            dismissDialog()
            dialog = SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE)
            dialog!!.progressHelper.barColor = Color.parseColor("#A5DC86")
            dialog!!.titleText = text
            dialog!!.setCancelable(cancelable)
            dialog!!.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun errorAlert(title: String, text: String) {
        dismissDialog()
        dialog = SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
        dialog!!.titleText = title
        dialog!!.contentText = text
        dialog!!.setCancelable(true)
        dialog!!.show()
    }

    fun infoAlert(title: String, text: String) {
        dismissDialog()
        dialog = SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
        dialog!!.titleText = title
        dialog!!.contentText = text
        dialog!!.setCancelable(false)
        dialog!!.show()
    }

    fun successAlert(title: String, text: String, activity : AppCompatActivity? = null) {
        dismissDialog()
        dialog = SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
        dialog!!.titleText = title
        dialog!!.contentText = text
        dialog!!.setCancelable(false)
        if (activity != null)
            dialog!!.setConfirmClickListener { sDialog ->
                    sDialog.cancel()
                activity.onBackPressed()
            }
        dialog!!.show()
    }

    fun dismissDialog() {
        try {
            if (dialog != null)
                if (dialog!!.isShowing) {
                    dialog!!.dismiss()
                }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    class Alert(var type : String? = null, var title : String? = null,var text : String? = null,var back : Boolean = false)
}